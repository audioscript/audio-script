parser grammar AudioScriptParser;

options { language=JavaScript; tokenVocab=AudioScriptLexer; }

document 
    : element+ EOF?
    ;

element
	: command=PROD_COMMAND id=ID file=(FILE | TEXT) (WITH mods+=modification (AND mods+=modification)*)? #Produce
	| WAIT FOR id=ID #Close
	| DEFINE ACTOR id=ID voice=VOICE #Define
	| COMMENT END_COMMENT #Comment
	;

modification returns[mod]
    : VOLUME value=FLOAT #Volume
    | LOOP #Loop
    | RATE value=FLOAT #Rate
    ;

