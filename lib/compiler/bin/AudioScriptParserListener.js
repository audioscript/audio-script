// Generated from AudioScriptParser.g4 by ANTLR 4.5.1
// jshint ignore: start
var antlr4 = require('antlr4/index');

// This class defines a complete listener for a parse tree produced by AudioScriptParser.
function AudioScriptParserListener() {
	antlr4.tree.ParseTreeListener.call(this);
	return this;
}

AudioScriptParserListener.prototype = Object.create(antlr4.tree.ParseTreeListener.prototype);
AudioScriptParserListener.prototype.constructor = AudioScriptParserListener;

// Enter a parse tree produced by AudioScriptParser#document.
AudioScriptParserListener.prototype.enterDocument = function(ctx) {
};

// Exit a parse tree produced by AudioScriptParser#document.
AudioScriptParserListener.prototype.exitDocument = function(ctx) {
};


// Enter a parse tree produced by AudioScriptParser#Produce.
AudioScriptParserListener.prototype.enterProduce = function(ctx) {
};

// Exit a parse tree produced by AudioScriptParser#Produce.
AudioScriptParserListener.prototype.exitProduce = function(ctx) {
};


// Enter a parse tree produced by AudioScriptParser#Close.
AudioScriptParserListener.prototype.enterClose = function(ctx) {
};

// Exit a parse tree produced by AudioScriptParser#Close.
AudioScriptParserListener.prototype.exitClose = function(ctx) {
};


// Enter a parse tree produced by AudioScriptParser#Define.
AudioScriptParserListener.prototype.enterDefine = function(ctx) {
};

// Exit a parse tree produced by AudioScriptParser#Define.
AudioScriptParserListener.prototype.exitDefine = function(ctx) {
};


// Enter a parse tree produced by AudioScriptParser#Comment.
AudioScriptParserListener.prototype.enterComment = function(ctx) {
};

// Exit a parse tree produced by AudioScriptParser#Comment.
AudioScriptParserListener.prototype.exitComment = function(ctx) {
};


// Enter a parse tree produced by AudioScriptParser#Volume.
AudioScriptParserListener.prototype.enterVolume = function(ctx) {
};

// Exit a parse tree produced by AudioScriptParser#Volume.
AudioScriptParserListener.prototype.exitVolume = function(ctx) {
};


// Enter a parse tree produced by AudioScriptParser#Loop.
AudioScriptParserListener.prototype.enterLoop = function(ctx) {
};

// Exit a parse tree produced by AudioScriptParser#Loop.
AudioScriptParserListener.prototype.exitLoop = function(ctx) {
};


// Enter a parse tree produced by AudioScriptParser#Rate.
AudioScriptParserListener.prototype.enterRate = function(ctx) {
};

// Exit a parse tree produced by AudioScriptParser#Rate.
AudioScriptParserListener.prototype.exitRate = function(ctx) {
};



exports.AudioScriptParserListener = AudioScriptParserListener;