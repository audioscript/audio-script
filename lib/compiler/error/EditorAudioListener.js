var ErrorListener = require('antlr4/error/ErrorListener').ErrorListener;

function EditorErrorListener() {
    ErrorListener.call(this);
    return this;
}

EditorErrorListener.prototype = Object.create(ErrorListener.prototype);
EditorErrorListener.prototype.constructor = EditorErrorListener;

//
// Provides a default instance of {@link EditorErrorListener}.
//
EditorErrorListener.INSTANCE = new EditorErrorListener();

//
// {@inheritDoc}
//
// <p>
// This implementation prints messages to {@link System//err} containing the
// values of {@code line}, {@code charPositionInLine}, and {@code msg} using
// the following format.</p>
//
// <pre>
// line <em>line</em>:<em>charPositionInLine</em> <em>msg</em>
// </pre>
//
EditorErrorListener.prototype.syntaxError = function(recognizer, offendingSymbol, line, column, msg, e) {
    var errorDiv = $('#errors');
    var html =
    `<div class="alert alert-danger alter-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        Line: <span class="label label-info">${line}</span>
        Col: <span class="label label-primary">${column}</span>
        ${msg}
    </div>`;
    errorDiv.html(html);
    editor.selection.moveTo(line - 1, column);
    throw line + ":" + column + " " + msg + offendingSymbol;
};

exports.EditorErrorListener = EditorErrorListener;