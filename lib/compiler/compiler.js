"use strict";
var antlr4 = require('antlr4/index');
var AudioScriptLexer = require('lib/compiler/bin/AudioScriptLexer').AudioScriptLexer;
var AudioScriptParser = require('lib/compiler/bin/AudioScriptParser').AudioScriptParser;
var AudioScriptParserListener = require('lib/compiler/bin/AudioScriptParserListener').AudioScriptParserListener;
var EditorErrorListener = require('/lib/compiler/error/EditorAudioListener').EditorErrorListener;

/**
 * Scene is an object that keeps track of all audio
 * elements as well as the commands required to layer
 * the scene.
 * @returns {Scene}
 * @constructor
 */
function Scene() {
    this._howls = new Map();
    this._actorHowls = new Map();
    this._actors = new Map();
    this._playActions = [];
    this._playPromise = null;
    return this;
}

/**
 * Add a howl object to the scene
 * @param name The id of the howl
 * @param howl The howl object
 */
Scene.prototype.addHowl = function(name, howl) {
    if(this._howls.has(name)) {
        throw new Error("'" + name + "' is already defined.");
    }

    console.info("Added howl: " + name);
    this._howls.set(name, howl);
};

/**
 * Add a command to be played during scene playback.
 * @param name The name of the howler
 * @param action The action to perform
 *  play - begin playing
 *  wait - wait until the named howler is done before continuing
 *  with scene actions
 */
Scene.prototype.addPlayCommand = function(name, action) {
    if(action != "play" && action != "wait") {
        throw new Error("Action must be play or wait.");
    }

    if(!this._howls.has(name)) {
        throw new Error("'" + name + "'" + ' has not been defined.');
    }

    var o = {};
    o.action = action;
    o.name = name;
    console.info("Added play command: " + o.action + " " + o.name);

    this._playActions.push(o);
};

/**
 * Define an actor in the scene.
 *
 * @param name The name of the actor. This must be unique to the scene.
 * @param voice The voice of the actor to use.
 */
Scene.prototype.addActor = function(name, voice) {
    if(this._actorHowls.has(name)) {
        throw new Error("Actor " + name + " is already defined.");
    }

    this._actorHowls.set(name, []);
    this._actors.set(name, voice);
};

/**
 * Play all of the scene commands
 * @returns {*} A deferred promise that will
 * be resolved when the scene has finished playing
 */
Scene.prototype.play = function() {
    if(this._playPromise != null) {
        throw "Scene is already playing";
    }

    this._playPromise = Q.defer();
    var scene = this;
    // Begin playing after return.
    setTimeout(() => scene._playStep(), 100);
    return this._playPromise.promise;
};

/**
 * Play as many commands as possible until we are out of
 * commands or we hit a wait command.
 * @private
 */
Scene.prototype._playStep = function() {
    while(this._playActions.length > 0) {
        var action = this._playActions.shift();
        console.info("Playing step: " + action.action + " " + action.name);
        var howl = this._howls.get(action.name);
        if(action.action == "play") {
            // We just want to begin playing
            howl.play();
        } else {
            // We want to currently stop resolving play
            // steps until the named audio has finished playing.
            if(howl.playing()) {
                var scene = this;
                howl.loop(false);
                howl.once(
                    'end', function() {
                        scene._playStep();
                    }
                );
                return;
            }
        }
    }

    // Force stop any howlers that didn't have explicit
    // wait rules.
    for(var howl of this._howls.values()) {
        howl.stop();
    }

    // All the actions have been resolved!
    this._playPromise.resolve();
    this._playPromise = null;
};

/**
 * Load all of the audio files into memory if they
 * have not already been loaded. Beginning the load of lazy-loaded
 * howlers.
 * @returns {*} A promise to load all of the memory files.
 */
Scene.prototype.load = function() {
    var defer = Q.defer();

    var loadCount = this._howls.size;
    var scene = this;
    var loadListener = function() {
        loadCount--;
        // This audio was the last thing
        // to load. We are ready to begin playing.
        if(loadCount == 0) {
            defer.resolve(scene);
        }
    };

    for(var howl of this._howls.values()) {
        if(howl.state() == "loaded") {
            // The howl was already loaded
            loadCount--;
        } else {
            // Add the listener to attempt to
            // resolve the load promise on load
            howl.once('load', loadListener);
            howl.onloaderror = function(error) {
                defer.reject(error);
                console.info("CAT!");
                throw new Error(error);
            };
        }
    }

    // We loaded everything without needing to promise!
    if(loadCount == 0) {
        setTimeout(defer.resolve(this), 100);
    }
    return defer.promise;

};
Scene.prototype.constructor = Scene;


/**
 * Construct the compiler for AudioScript.
 * @returns {AudioScriptCompiler}
 * @constructor
 */
function AudioScriptCompiler() {
    AudioScriptParserListener.call(this);
    return this;
}

AudioScriptCompiler.prototype = Object.create(AudioScriptParserListener.prototype);
AudioScriptCompiler.prototype.constructor = AudioScriptCompiler;

/**
 * Compile the script into a scene.
 *
 * @param template The script.
 * @returns A deferred promise to return a scene when all text
 * to speech has been created.
 */
AudioScriptCompiler.prototype.compile = function (template) {
    console.info("Compile");
    this._scene = new Scene();

    this._compilePromise = Q.defer();

    // The number of text-to-audio that still needs to
    // be converted.
    this._tts = 0;

    // Whether the tts callbacks can resolve the promise.
    // This is set to true after we have walked the tree.
    this._isCompilePromiseResolvable =  false;

    var chars = new antlr4.InputStream(template);
    var lexer = new AudioScriptLexer(chars);
    var tokens = new antlr4.CommonTokenStream(lexer);
    var parser = new AudioScriptParser(tokens);

    console.info("Removing Listeners");
    lexer.removeErrorListeners();
    parser.removeErrorListeners();

    lexer.addErrorListener(EditorErrorListener.INSTANCE);
    parser.addErrorListener(EditorErrorListener.INSTANCE);

    console.info("About to Document");
    parser.buildParseTrees = true;
    var tree = parser.document();
    console.info("Documented");

    console.info("About to Walk");
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);
    console.info("Walked");

    var compiler = this;
    if(this._tts == 0) {
        // We aren't waiting on any tts callbacks. Resolve immediately.
        setTimeout(function() { compiler._compilePromise.resolve(compiler._scene);}, 500);
    }
    this._isCompilePromiseResolvable = true;

    return this._compilePromise.promise;
};

AudioScriptCompiler.prototype.displayError = function(ctx, error) {
    var errorDiv = $('#errors');
    var html =
        `<div class="alert alert-danger alter-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        Line: <span class="label label-info">${ctx.start.line}</span>
        Col: <span class="label label-primary">${ctx.start.column}</span>
        ${error}
        </div>`;
        errorDiv.html(html);

    this._compilePromise.reject(error);
    throw error;
};

AudioScriptParserListener.prototype.exitProduce = function (ctx) {
    var fileText = ctx.file.text;

    var options = [];
    var id = null;
    try {
        if(fileText.charAt(0) == '"' && fileText.charAt(ctx.file.length - 1)) {
            // The command was for a tts.

            var actorId = ctx.id.text;

            // Generate a new uuid for the tts. We can't use the actor-id as an actor
            // may say multiple things.
            id = uuid.v1();
            var compiler = this;
            if(!this._scene._actors.has(actorId)) {
                throw "Actor " + actorId + " has not been defined."
            }

            // Make a request against the node server to generate a .wav file representative
            // of the tts for the actor and store it under tmp/uuid.wav
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/tts',
                contentType: 'application/json; charset=UTF-8',
                data: JSON.stringify({
                    "voice" : this._scene._actors.get(actorId),
                    "id" : id,
                    "text" : fileText.replace(/['"]+/g, '')
                }),
                complete: function(data, status) {
                    // The tts has been completed.
                    compiler._tts--;
                    if(compiler._tts == 0 && compiler._isCompilePromiseResolvable) {
                        // The compiler was waiting on this tts to resolve before
                        // resolving the scene to the callee. So resolve the scene.
                        compiler._compilePromise.resolve(compiler._scene);
                    }
                    // The howl was lazy-loaded on construction. But now that the audio
                    // file exists we can begin to load into cache.
                    compiler._scene._howls.get(id).load();
                }
            });

            // Add the id of this howl to the list of howls that
            // are bound to the given actor.
            var scene = this._scene;
            scene._actorHowls.get(actorId).push(id);
            options['onend'] = function(sound) {
                var index = scene._actorHowls.get(actorId).indexOf(id);
                scene._actorHowls.get(actorId).slice(index, 1);
            };

            // There is now 1 more tts that needs to be resolved before
            // the compiler can resolve a scene to the callee.
            this._tts++;

            // Lazy-load (the audio file doesn't exist yet)
            options["preload"] = false;
            options["src"] = [`tmp/${id}.wav`];
        } else {
            // The audio file already exists. Begin
            // loading it into memory immediately.
            id =  ctx.id.text;
            options["src"] = [fileText];
        }

        // We want to perform modifications against the howl object
        if(ctx.mods.length > 0) {
            for(var i = 0; i < ctx.mods.length; i++) {
                var mod = ctx.mods[i].mod;
                options[mod.type] = mod.value;
            }
        }

        var compiler = this;
        options['onloaderror'] = function(error) {
            compiler.displayError(ctx, new Error(`Failed to load audio file.${options['src'][0]}`, error));
        };

        this._scene.addHowl(id, new Howl(options));
        this._scene.addPlayCommand(id, "play");
    } catch(err) {
        this.displayError(ctx, err);
    }
};

AudioScriptParserListener.prototype.exitClose = function(ctx) {
    try {
        var id = ctx.id.text;
        if(this._scene._actorHowls.has(id)) {
            // We want to wait for all the audio for a speaker
            for(var howl of this._scene._actorHowls.get(id)) {
                this._scene.addPlayCommand(howl, "wait");
            }
        } else {
            this._scene.addPlayCommand(ctx.id.text, "wait");
        }

    } catch (error) {
        this.displayError(ctx, error);
    }
};

AudioScriptParserListener.prototype.exitDefine = function (ctx) {
    try {
        this._scene.addActor(ctx.id.text, ctx.voice.text);
    } catch(error) {
        this.displayError(ctx, error);
    }
};

AudioScriptParserListener.prototype.exitVolume = function(ctx) {
  try {
      var volume = parseFloat(ctx.value.text);
      if(volume < 0 || volume > 1) {
          throw new Error("Volume must be between [0, 1].");
      }
      ctx.mod = {};
      ctx.mod.value = volume;
      ctx.mod.type = 'volume';
  } catch (error) {
      this.displayError(ctx, error);
  }
};

AudioScriptParserListener.prototype.exitLoop = function(ctx) {
    ctx.mod = {};
    ctx.mod.type = 'loop';
    ctx.mod.value = true;
};

AudioScriptParserListener.prototype.exitRate = function(ctx) {
    try {
        var rate = parseFloat(ctx.value.text);
        if(rate < 0 || rate > 4) {
            throw new Error("Rate must be between [0, 4].");
        }
        ctx.mod = {};
        ctx.mod.value = rate;
        ctx.mod.type = 'rate';
    } catch (error) {
        this.displayError(ctx, error);
    }
};

exports.AudioScriptCompiler = AudioScriptCompiler;