var Compiler = require('lib/compiler/compiler.js').AudioScriptCompiler;
var Howler = require('node_modules/howler/dist/howler.min.js');
var editor;

$(document).ready(function() {
    editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");
    editor.setShowPrintMargin(false);

    $.getJSON(
        "/files", function(data) {
            var playTemplates = $('#playTemplate');
            for(var item of data.files) {
                playTemplates.append(
                    `<li><a onclick="insertPlayFile('${item}')">${item}</a></li>`
                );
            }
        }
    );
});

function insertPlayFile(playFile) {
    var pureFile = playFile.substring(playFile.lastIndexOf("/") + 1, playFile.length ).slice(0, -4);
    insertAtCursor(`PLAY ${pureFile} ${playFile} WAIT FOR ${pureFile}`);

};

function insertAtCursor(text) {
    var cursor =  editor.selection.getCursor();
    var pos = {row:cursor.row, column: cursor.column};
    editor.session.insert(pos, text + '\n');
}


function buttonPlayPress(element) {
    element.children().removeClass('fa-play');
    element.children().addClass('fa-gear fa-spin');
    element.attr('disabled', 'disabled');

    var script = editor.getValue();
    console.info(script);

    $('#errors').html('');

    try {
        var compiler = new Compiler();
        compiler.compile(script).then(
            (scene) => {
                console.info("LOAD");
                element.children().removeClass('fa-gear fa-spin');
                element.children().addClass('fa-cloud-download');
                return scene.load();
            }
        ).then(
            (scene) => {
                console.info("PLAY");
                element.children().removeClass('fa-cloud-download');
                element.children().addClass('fa-volume-up');
                return scene.play()
            }
        ).then(
            () => {
                console.info("COMPLETE");
                element.children().removeClass('fa-volume-up');
                element.children().addClass('fa-play');
                element.removeAttr('disabled');
            }
        ).catch(
            (error) => {
                console.info("Async error.");
                console.info(error);
                element.children().removeClass('fa-gear fa-spin');
                element.children().addClass('fa-play');
                element.removeAttr('disabled');
            }
        );
    } catch (err) {
        console.info(err);
        element.children().removeClass('fa-gear fa-spin');
        element.children().addClass('fa-play');
        element.removeAttr('disabled');
    }
}
